// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_StateEffect.h"
#include "GameFramework/Actor.h"


bool UTDS_StateEffect::InitObject(AActor* Actor)
{
	FRotator addRot = FRotator(0, 3, 0);
	Actor->AddActorWorldRotation(addRot);

	return true;
}

void UTDS_StateEffect::DestroyObject()
{
}
