// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Weapon/WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMeshActor.h"
#include "Math/Rotator.h"
#include "Runtime/Engine/Public/EngineGlobals.h"
#include "TDS/Character/TDSInventoryComponent.h"
#include "TDS/Interface/TDS_IGameActor.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Shoot Location"));
	ShootLocation->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
	
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	ShellDropTick(DeltaTime);
	ClipDropTick(DeltaTime);

}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (CheckWeaponCanFire())
	{
		if (!WeaponReloading && WeaponFiring)
		{
			if (GetWeaponRound() > 0)
			{
				if (FireTimer < 0)
				{
					Fire();
				}
			}
			else
			{
				InitReload();
			}
		}

		/*if (FireTimer >= 0)
		{
			FireTimer -= DeltaTime;
		}*/
	}
	/*else if (FireTimer >= 0)
	{
		FireTimer -= DeltaTime;
	}*/

	/*if (CheckWeaponCanFire())
	{
		if (FireTimer < 0 && !WeaponReloading && WeaponFiring && GetWeaponRound() > 0)
		{
			Fire();
		}
	}*/

	if (FireTimer >= 0)
	{
		FireTimer -= DeltaTime;
	}

}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (CheckWeaponCanFire())
	{
		//if (!WeaponReloading)
		//{
			//if (WeaponFiring)
			//{
				if (ShouldReduceDispersion)
				{
					if (CurrentDispersion > CurrentDispersionMin)
					{
						CurrentDispersion -= CurrentDispersionReduction;
					}
					else
					{
						CurrentDispersion = CurrentDispersionMin;
					}
				}
				else
				{
					if (CurrentDispersion < CurrentDispersionMax)
					{
						CurrentDispersion += CurrentDispersionReduction;
					}
					else
					{
						CurrentDispersion = CurrentDispersionMax;
					}
				}
			//}
		//}
	}
	if (ShowDebug)
	{
		GEngine->AddOnScreenDebugMessage(0, 0, FColor::Red, FString::Printf(TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion));
	}
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
		SkeletalOrStatic01 = 1;
	}
	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
		SkeletalOrStatic01 = 0;
	}
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
	{
		WeaponFiring = bIsFire;
	}
	else
	{
		WeaponFiring = false;
	}
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

/*bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;
	if (GetOwner())
	{
		UTDSInventoryComponent* MyInv = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if (MyInv)
		{
			int8 AviableAmmoForWeapon;
			if (!MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AviableAmmoForWeapon))
			{
				result = false;
			}
		}
	}

	return result;
}*/

int32 AWeaponDefault::GetAviableAmmoForReload()
{
	int32 AviableAmmoForWeapon = 1;//WeaponSetting.MaxRound;
	if (GetOwner())
	{
		UTDSInventoryComponent* MyInv = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if (MyInv)
		{
			AviableAmmoForWeapon = MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType);
		}
	}
	return AviableAmmoForWeapon;
}

void AWeaponDefault::Fire()
{
	FireTimer = WeaponSetting.RateOfFire;
	WeaponInfo.Round--;


	if (WeaponSetting.ShellBullets.DropMesh)
	{
		if (WeaponSetting.ShellBullets.DropMeshTime < 0)
		{
			InitDropMesh(
				WeaponSetting.ShellBullets.DropMesh, 
				WeaponSetting.ShellBullets.DropMeshOffset, 
				WeaponSetting.ShellBullets.DropMeshImpulseRot,
				WeaponSetting.ShellBullets.ImpulseRandomDispersion, 
				WeaponSetting.ShellBullets.DropMeshLifeTime, 
				WeaponSetting.ShellBullets.PowerImpulse, 
				WeaponSetting.ShellBullets.CustomMass);
		}
		else
		{
			DropShellFlag = true;
			DropShellTimer = WeaponSetting.ShellBullets.DropMeshTime;
		}
	}

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo = GetProjectile();
		FVector EndLocation = FVector(0, 0, 0);

		if (WeaponSetting.SoundFireWeapon)
		{
			UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, SpawnLocation);
		}
		if (WeaponSetting.EffectFireWeapon)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, SpawnLocation, SpawnRotation, true);
		}
		OnWeaponFireStart.Broadcast();

		if (!SkeletalOrStatic01 && WeaponSetting.AnimWeaponFire)
		{
			SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponSetting.AnimWeaponFire);
		}

		int32 NumberProjectile = GetNumberProjectileByShot();

		for (int32 i = 0; i <= NumberProjectile - 1; i++)
		{
			EndLocation = GetFireEndLocation();
			FVector Dir = EndLocation - SpawnLocation;
			Dir.Normalize();

			if (ProjectileInfo.Projectile)
			{
				FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
					//ToDo Init Projectile settings by id in table row(or keep in weapon table)
					//myProjectile->InitialLifeSpan = 20.0f;
					//Projectile->BulletProjectileMovement->InitialSpeed = 2500.0f;
				}
			}
			else
			{
				FHitResult Hit;
				TArray<AActor*> Actors;

				UKismetSystemLibrary::LineTraceSingle(GetWorld(), 
					SpawnLocation, 
					SpawnLocation + (Dir * WeaponSetting.DistanceTrace),
					ETraceTypeQuery::TraceTypeQuery4, 
					false, 
					Actors, 
					EDrawDebugTrace::ForDuration, 
					Hit, 
					true, 
					FLinearColor::Red,
					FLinearColor::Green, 
					5);

				/*if (ShowDebug)
				{
					DrawDebugLine(GetWorld(),
						SpawnLocation, 
						SpawnLocation + ShootLocation->GetForwardVector() * WeaponSetting.DistanceTrace, 
						FColor::Black, 
						false, 5, (uint8)'\000', 0.5);
				}*/
				
				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

					if (WeaponSetting.ProjectileSetting.HitDecals.Contains(mySurfacetype))
					{
						UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[mySurfacetype];

						if (myMaterial && Hit.GetComponent())
						{
							UGameplayStatics::SpawnDecalAttached(myMaterial, 
								FVector(20), 
								Hit.GetComponent(), 
								NAME_None, 
								Hit.ImpactPoint, 
								Hit.ImpactNormal.Rotation(), 
								EAttachLocation::KeepWorldPosition, 10.0f);
						}
					}

					if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurfacetype))
					{
						UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.HitFXs[mySurfacetype];

						if (myParticle && Hit.GetComponent())
						{
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),
								myParticle,
								FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
						}
					}

					if (WeaponSetting.ProjectileSetting.HitSound)
					{
						UGameplayStatics::PlaySoundAtLocation(GetWorld(),
							WeaponSetting.ProjectileSetting.HitSound,
							Hit.ImpactPoint);
					}


					UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileInfo.Effect, mySurfacetype);


					//��� ���������� ������� ���� Native � Implementable �� ���������� ������� ������� (�������� Hit.GetActor())
					/*
					if (Hit.GetActor()->GetClass()->ImplementsInterface(UTDS_IGameActor::StaticClass()))
					{
						ITDS_IGameActor::Execute_AviableForEffects(Hit.GetActor());
						ITDS_IGameActor::Execute_AviableForEffectsBP(Hit.GetActor());
					}*/

					//� ��� ���������� ������� ����� ���������� �� ���������� ������� ������� (�������� Hit.GetActor())
					/*ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(Hit.GetActor());
					if (myInterface)
					{
						myInterface->AviableForEffectsOnlyCPP();
					}*/

					UGameplayStatics::ApplyDamage(
						Hit.GetActor(),
						WeaponSetting.ProjectileSetting.ProjectileDamage,
						GetInstigatorController(),
						this, NULL);
				}
			}
		}
		ChangeDispersionByShoot();
	}

	if (!(GetWeaponRound() > 0) && !WeaponReloading)
	{
		InitReload();
	}
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_StateDispersionReduction;
		break;
	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Run_StateDispersionReduction;
		break;
	case EMovementState::AimWalk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionReduction;
		break;
	case EMovementState::Sprint_State:
		BlockFire = true;
		SetWeaponStateFire(false);
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShoot()
{
	if (CurrentDispersion < CurrentDispersionMax)
	{
		CurrentDispersion += CurrentDispersionRecoil;
	}
}

void AWeaponDefault::InitDropMesh(
	UStaticMesh* DropMesh,
	FTransform Offset,
	FRotator DropImpulseRotation,
	float ImpulseRandomDispersion,
	float LifeTimeMesh,
	float PowerImpulse,
	float CustomMass)
{
	if (DropMesh)
	{
		FTransform Transform;
		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X + this->GetActorRightVector() * Offset.GetLocation().Y + this->GetActorUpVector() * Offset.GetLocation().Z;
		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());
		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());
		AStaticMeshActor* NewActor = nullptr;
		FActorSpawnParameters Param;
		Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		Param.Owner = this;
		NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Param);

		if (NewActor && NewActor->GetStaticMeshComponent())
		{
			NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

			NewActor->SetActorTickEnabled(false);
			NewActor->InitialLifeSpan = LifeTimeMesh;

			NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);
			
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

			if (CustomMass > 0)
			{
				NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
			}

			//if (FMath::IsNearlyZero(ImpulseRandomDispersion))
			//{
				FRotator FinalRot = GetActorRotation() + DropImpulseRotation;
				FVector FinalDir = FinalRot.RotateVector(FVector(1, 0, 0));
				FinalDir = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(FinalDir, ImpulseRandomDispersion);
				FinalDir = FinalDir.GetSafeNormal();
				NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir * PowerImpulse);
			//}
		}
	}
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if (DropShellFlag)
	{
		if (DropShellTimer < 0)
		{
			DropShellFlag = false;
			InitDropMesh(
				WeaponSetting.ShellBullets.DropMesh,
				WeaponSetting.ShellBullets.DropMeshOffset,
				WeaponSetting.ShellBullets.DropMeshImpulseRot,
				WeaponSetting.ShellBullets.ImpulseRandomDispersion,
				WeaponSetting.ShellBullets.DropMeshLifeTime,
				WeaponSetting.ShellBullets.PowerImpulse,
				WeaponSetting.ShellBullets.CustomMass);
		}
		else
		{
			DropShellTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::ClipDropTick(float DeltaTime)
{
	if (DropClipFlag)
	{
		if (DropClipTimer < 0)
		{
			DropClipFlag = false;
			InitDropMesh(
				WeaponSetting.ClipDropMesh.DropMesh,
				WeaponSetting.ClipDropMesh.DropMeshOffset,
				WeaponSetting.ClipDropMesh.DropMeshImpulseRot,
				WeaponSetting.ClipDropMesh.ImpulseRandomDispersion,
				WeaponSetting.ClipDropMesh.DropMeshLifeTime,
				WeaponSetting.ClipDropMesh.PowerImpulse,
				WeaponSetting.ClipDropMesh.CustomMass);
		}
		else
		{
			DropClipTimer -= DeltaTime;
		}
	}
}

int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponInfo.Round;
}

int32 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	return CurrentDispersion;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180);
}

FVector AWeaponDefault::GetFireEndLocation()
{
	FVector EndLocation = FVector(0, 0, 0);
	FVector tmpV = ShootEndLocation - ShootLocation->GetComponentLocation();
	
	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((tmpV).GetSafeNormal());
		if (ShowDebug)
		{
			DrawDebugCone(GetWorld(), 
				ShootLocation->GetComponentLocation(), 
				tmpV,
				WeaponSetting.DistanceTrace, 
				GetCurrentDispersion() * PI / 180, 
				GetCurrentDispersion() * PI / 180, 
				32, FColor::Emerald, false, 0.1, (uint8)'\000', 1);
		}
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector());
		if (ShowDebug)
		{
			DrawDebugCone(GetWorld(), 
				ShootLocation->GetComponentLocation(), 
				ShootLocation->GetForwardVector(), 
				WeaponSetting.DistanceTrace,
				GetCurrentDispersion() * PI / 180, 
				GetCurrentDispersion() * PI / 180, 
				32, FColor::Emerald, false, 0.1, (uint8)'\000', 1);
		}
	}

	return EndLocation;
}

void AWeaponDefault::InitReload()
{
	if ((GetWeaponRound() < WeaponSetting.MaxRound) &&
		!WeaponReloading &&
		/*CheckCanWeaponReload()*/
		(GetAviableAmmoForReload() > 0))
	{
		WeaponReloading = true;
		ReloadTimer = WeaponSetting.ReloadTime;

		//����� ����� ����� �������� �������� �����������
		OnWeaponReloadStart.Broadcast();
		/*if (!SkeletalOrStatic01 && WeaponSetting.AnimWeaponReload)
		{
			SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(
				WeaponSetting.AnimWeaponReload,
				WeaponSetting.AnimWeaponReload->SequenceLength / WeaponSetting.ReloadTime);
		}*/

		if (WeaponSetting.ClipDropMesh.DropMesh)
		{
			if (WeaponSetting.ClipDropMesh.DropMeshTime < 0)
			{
				InitDropMesh(
					WeaponSetting.ClipDropMesh.DropMesh,
					WeaponSetting.ClipDropMesh.DropMeshOffset,
					WeaponSetting.ClipDropMesh.DropMeshImpulseRot,
					WeaponSetting.ClipDropMesh.ImpulseRandomDispersion,
					WeaponSetting.ClipDropMesh.DropMeshLifeTime,
					WeaponSetting.ClipDropMesh.PowerImpulse,
					WeaponSetting.ClipDropMesh.CustomMass);
			}
			else
			{
				DropClipFlag = true;
				DropClipTimer = WeaponSetting.ClipDropMesh.DropMeshTime;
			}
		}
	}
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	int32 OldRound = WeaponInfo.Round;
	int32 AviableRound = GetAviableAmmoForReload();
	if (OldRound + AviableRound > WeaponSetting.MaxRound)
	{
		WeaponInfo.Round = WeaponSetting.MaxRound;
	}
	else
	{
		WeaponInfo.Round = OldRound + AviableRound;
	}

	OnWeaponReloadEnd.Broadcast(true, OldRound);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15);
	}

	OnWeaponReloadEnd.Broadcast(false, 0);
	DropClipFlag = false;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

