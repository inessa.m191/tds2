// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "TDS/FuncLibrary/Types.h"
#include "TDS/Weapon/ProjectileDefault.h"
#include "WeaponDefault.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponReloadStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponReloadStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, OldRound);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponFireStart);

UCLASS()
class TDS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponFireStart OnWeaponFireStart;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY()
		FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
		FAddicionalWeaponInfo WeaponInfo;

	bool SkeletalOrStatic01 = 0; // 0 - ������, 1 - ������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool WeaponFiring = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		bool WeaponReloading = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool ShowDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		float SizeVectorToChangeShootDirectionLogic = 100.0f;

	bool BlockFire = false;

	bool DropShellFlag = false;
	bool DropClipFlag = false;
	float DropShellTimer = -1;
	float DropClipTimer = -1;

	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0;
	float CurrentDispersionMax = 1;
	float CurrentDispersionMin = 0.1;
	float CurrentDispersionRecoil = 0.1;
	float CurrentDispersionReduction = 0.1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		float FireTimer = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		float ReloadTimer = 0;


	FVector ShootEndLocation = FVector(0, 0, 0);

public:

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);

	void WeaponInit();

	UFUNCTION(BlueprintCallable)
		void SetWeaponStateFire(bool bIsFire);

	bool CheckWeaponCanFire();
	//bool CheckCanWeaponReload();
	int32 GetAviableAmmoForReload();

	void Fire();

	void UpdateStateWeapon(EMovementState NewMovementState);

	void ChangeDispersionByShoot();

	void InitDropMesh(
		UStaticMesh* DropMesh,
		FTransform Offset,
		FRotator DropImpulseRotation,
		float LifeTimeMesh,
		float ImpulseRandomDispersion,
		float PowerImpulse,
		float CustomMass);
	void ShellDropTick(float DeltaTime);
	void ClipDropTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
		int32 GetWeaponRound();

	int32 GetNumberProjectileByShot() const;

	float GetCurrentDispersion() const;

	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;

	FVector GetFireEndLocation();

	UFUNCTION()
	void InitReload();
	void FinishReload();
	void CancelReload();

	FProjectileInfo GetProjectile();

};
