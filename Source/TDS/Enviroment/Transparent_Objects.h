// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Transparent_Objects.generated.h"

class UStaticMeshComponent;
class UMaterialInterface;

UCLASS()
class TDS_API ATransparent_Objects : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATransparent_Objects();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY()
		TArray<UMaterialInterface*> StarterMaterials;
	UPROPERTY()
		bool FirstOverlap;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UMaterialInterface* TransparentMaterial_00;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UMaterialInterface* TransparentMaterial_01;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UMaterialInterface* TransparentMaterial_02;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UMaterialInterface* TransparentMaterial_03;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UMaterialInterface* TransparentMaterial_04;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UMaterialInterface* TransparentMaterial_05;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UMaterialInterface* TransparentMaterial_06;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UMaterialInterface* TransparentMaterial_07;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UMaterialInterface* TransparentMaterial_08;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UMaterialInterface* TransparentMaterial_09;

	UPROPERTY()
		TArray<UMaterialInterface*> TransparentMaterials;
	UPROPERTY()
		int32 NumAllTransparentMaterial;
	UPROPERTY()
		int32 NumThisTransparentMaterial;

	UPROPERTY(EditDefaultsOnly)
		float TransparentTime;

	FTimerHandle TimerTransparent;
	FTimerHandle TimerNotTransparent;

	UFUNCTION()
		void HandleBeginOverlap(
			UPrimitiveComponent* OverlapedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);

	UFUNCTION()
		void HandleEndOverlap(
			UPrimitiveComponent* OverlappedComp, 
			AActor* OtherActor, 
			UPrimitiveComponent* OtherComp, 
			int32 OtherBodyIndex);

	UFUNCTION()
		void TransparentObject();

	UFUNCTION()
		void NotTransparentObject();
};
