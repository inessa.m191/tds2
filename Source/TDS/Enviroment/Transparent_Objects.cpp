// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Enviroment/Transparent_Objects.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "TDS/Character/TDSCharacter.h"
#include "Engine/Classes/Materials/MaterialInterface.h"
#include "Kismet/KismetSystemLibrary.h"
#include "TDS/Enviroment/CCylinder.h"

// Sets default values
ATransparent_Objects::ATransparent_Objects()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Block);

	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ATransparent_Objects::HandleBeginOverlap);
	MeshComponent->OnComponentEndOverlap.AddDynamic(this, &ATransparent_Objects::HandleEndOverlap);

	FirstOverlap = true;
	NumThisTransparentMaterial = -1;
	TransparentTime = 0.1;
}

// Called when the game starts or when spawned
void ATransparent_Objects::BeginPlay()
{
	Super::BeginPlay();
	TransparentMaterials = {
	TransparentMaterial_00,
	TransparentMaterial_01,
	TransparentMaterial_02,
	TransparentMaterial_03,
	TransparentMaterial_04,
	TransparentMaterial_05,
	TransparentMaterial_06,
	TransparentMaterial_07,
	TransparentMaterial_08,
	TransparentMaterial_09
	};
}

// Called every frame
void ATransparent_Objects::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATransparent_Objects::HandleBeginOverlap(
	UPrimitiveComponent* OverlapedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	ACCylinder* Character = Cast<ACCylinder>(OtherActor);
	if (IsValid(Character))
	{
		if (FirstOverlap)
		{
			StarterMaterials = MeshComponent->GetMaterials();
			NumAllTransparentMaterial = TransparentMaterials.Num();

			FirstOverlap = false;
		}
		GetWorldTimerManager().ClearTimer(TimerNotTransparent);
		GetWorldTimerManager().SetTimer(TimerTransparent, this, &ATransparent_Objects::TransparentObject, TransparentTime / 10, true);
	}
}

void ATransparent_Objects::HandleEndOverlap(
	UPrimitiveComponent* OverlappedComp,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
	ACCylinder* Character = Cast<ACCylinder>(OtherActor);
	if (IsValid(Character))
	{
		GetWorldTimerManager().ClearTimer(TimerTransparent);
		GetWorldTimerManager().SetTimer(TimerNotTransparent, this, &ATransparent_Objects::NotTransparentObject, TransparentTime / 10, true);
	}
}

void ATransparent_Objects::TransparentObject()
{
	if (NumThisTransparentMaterial <= NumAllTransparentMaterial - 2)
	{
		NumThisTransparentMaterial = NumThisTransparentMaterial + 1;
		//UKismetSystemLibrary::PrintString(GetWorld(), FString::Printf(TEXT("%d"), NumThisTransparentMaterial));
		int32 NMaterials = StarterMaterials.Num();
		for (int i = 0; i < NMaterials; i++)
		{
			MeshComponent->SetMaterial(i, TransparentMaterials[NumThisTransparentMaterial]);
		}
	}
	else
	{
		GetWorldTimerManager().ClearTimer(TimerTransparent);
	}
}

void ATransparent_Objects::NotTransparentObject()
{
	if (NumThisTransparentMaterial > 0)
	{
		NumThisTransparentMaterial--;
		int32 NMaterials = StarterMaterials.Num();
		for (int i = 0; i < NMaterials; i++)
		{
			MeshComponent->SetMaterial(i, TransparentMaterials[NumThisTransparentMaterial]);
		}
	}
	else
	{
		int32 NMaterials = StarterMaterials.Num();
		for (int i = 0; i < NMaterials; i++)
		{
			MeshComponent->SetMaterial(i, StarterMaterials[i]);
		}
		GetWorldTimerManager().ClearTimer(TimerNotTransparent);
	}
}
