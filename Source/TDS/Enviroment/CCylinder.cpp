// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Enviroment/CCylinder.h"

// Sets default values
ACCylinder::ACCylinder()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACCylinder::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACCylinder::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

