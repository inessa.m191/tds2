// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TDS/Game/TDSGameInstance.h"
#include "TDS/Character/TDSInventoryComponent.h"
#include "TDS/Character/TDSCharHealthComponent.h"
#include "TDS/Weapon/ProjectileDefault.h"


ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	HealthComponent = CreateDefaultSubobject<UTDSCharHealthComponent>(TEXT("HealthComponent"));
	InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>(TEXT("InventoryComponent"));

	if (HealthComponent)
	{
		HealthComponent->OnHavingStamina.AddDynamic(this, &ATDSCharacter::ChangeCanSprint);
		HealthComponent->OnDead.AddDynamic(this, &ATDSCharacter::CharDead);
	}

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDSCharacter::InitWeapon);
	}

	// Create a decal in the world to show the cursor's location
	/*
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());
	*/

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	/*
	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
	*/

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	//InitWeapon(InitWeaponName);

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* newInputComponent)
{
	Super::SetupPlayerInputComponent(newInputComponent);
	newInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
	newInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);

	newInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputAttackPressed);
	newInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::InputAttackReleased);
	newInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TryReloadWeapon);

	newInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TrySwitchNextWeapon);
	newInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TrySwitchPreviosWeapon);

}

void ATDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDSCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATDSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	if (bIsAlive)
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

		if ((UKismetMathLibrary::Abs(AxisX) + UKismetMathLibrary::Abs(AxisY)) > 0 && 
			MovementState == EMovementState::Sprint_State)
		{
			HealthComponent->ChangeStaminaValue(-DeltaTime);
		}

		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (IsValid(myController))
		{
			FHitResult ResultHit;
			//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
			myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

			float FindYawRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindYawRotation, 0.0f)));

			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0, 0, 0);

				switch (MovementState)
				{
				case EMovementState::Aim_State:
					Displacement = FVector(0, 0, 160);
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0, 0, 65);
					break;
				case EMovementState::Run_State:
					Displacement = FVector(0, 0, 120);
					break;
				case EMovementState::AimWalk_State:
					Displacement = FVector(0, 0, 105);
					break;
				case EMovementState::Sprint_State:
					break;
				default:
					break;
				}

				CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
			}

			if (CurrentWeapon)
			{
				if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f))
				{
					CurrentWeapon->ShouldReduceDispersion = true;
				}
				else
				{
					CurrentWeapon->ShouldReduceDispersion = false;
				}
			}
		}
	}
	/*
	float FindYawRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), GetCursorToWorld()->GetComponentLocation()).Yaw;
	SetActorRotation(FQuat(FRotator(0.0f, FindYawRotation, 0.0f)));
	*/


}

void ATDSCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State: ResSpeed = MovementInfo.AimSpeed; break;
	case EMovementState::Walk_State: ResSpeed = MovementInfo.WalkSpeed; break;
	case EMovementState::Run_State: ResSpeed = MovementInfo.RunSpeed; break;
	case EMovementState::AimWalk_State: ResSpeed = MovementInfo.AimWalkSpeed; break;
	case EMovementState::Sprint_State: ResSpeed = MovementInfo.SprintSpeed; break;
	default: break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSCharacter::ChangeMovementState()
{
	UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::ChangeMovementState %s"), CanSprint? TEXT("true") : TEXT("false"));


	if (CanSprint)
	{
		if (!AimEnable && !WalkEnable && !SprintEnable)
		{
			MovementState = EMovementState::Run_State;
		}
		else
		{
			if (SprintEnable)
			{
				AimEnable = 0;
				WalkEnable = 0;
				MovementState = EMovementState::Sprint_State;
			}
			else
			{
				if (AimEnable && WalkEnable)
				{
					MovementState = EMovementState::AimWalk_State;
				}
				else
				{
					if (AimEnable && !WalkEnable)
					{
						MovementState = EMovementState::Aim_State;
					}
					if (WalkEnable && !AimEnable)
					{
						MovementState = EMovementState::Walk_State;
					}
				}
			}
		}
	}
	else
	{
		if (!AimEnable && !WalkEnable)
		{
			MovementState = EMovementState::Run_State;
		}
		else
		{
			if (AimEnable && WalkEnable)
			{
				MovementState = EMovementState::AimWalk_State;
			}
			else
			{
				if (AimEnable && !WalkEnable)
				{
					MovementState = EMovementState::Aim_State;
				}
				if (WalkEnable && !AimEnable)
				{
					MovementState = EMovementState::Walk_State;
				}
			}
		}
	}

	CharacterUpdate();

	//��������� �������� ������ (���� ��� �� ������)
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

UDecalComponent* ATDSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ATDSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo ��������: ������ �������� ��� ��� ��������
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
	}
}

AWeaponDefault* ATDSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDSCharacter::InitWeapon(FName IdWeaponName, FAddicionalWeaponInfo WeaponAddicionalInfo, int32 NewCurrentIndexWeapon)
{
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;

	if (CurrentWeapon)
	{
		if (CurrentWeapon->WeaponReloading)
		{
			CurrentWeapon->CancelReload();
		}
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					//myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
					myWeapon->WeaponInfo = WeaponAddicionalInfo;
					myWeapon->UpdateStateWeapon(MovementState);

					//if (InventoryComponent)
					//{
						CurrentIndexWeapon = NewCurrentIndexWeapon;//fix
					//}

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDSCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDSCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATDSCharacter::WeaponFireStart);

				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

void ATDSCharacter::TrySwitchNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		//int8 OldIndex = CurrentIndexWeapon;
		FAddicionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, CurrentIndexWeapon, OldInfo))
			{

			}
		}
	}
}

void ATDSCharacter::TrySwitchPreviosWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		//int8 OldIndex = CurrentIndexWeapon;
		FAddicionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, CurrentIndexWeapon, OldInfo))
			{

			}
		}
	}
}

void ATDSCharacter::TryReloadWeapon()
{
	if (CurrentWeapon /* &&
		(CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound) &&
		!CurrentWeapon->WeaponReloading &&
		CurrentWeapon->CheckCanWeaponReload()*/)
	{
		CurrentWeapon->InitReload();
	}
}

void ATDSCharacter::WeaponReloadStart()
{
	//������ ����� �������� � ������ ����� ��������
	if (AimEnable == true && CurrentWeapon->WeaponSetting.AnimCharReloadAim)
	{
		WeaponReloadStart_BP(CurrentWeapon->WeaponSetting.AnimCharReloadAim);

		if (!CurrentWeapon->SkeletalOrStatic01 && CurrentWeapon->WeaponSetting.AnimWeaponReload)
		{
			CurrentWeapon->SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(
				CurrentWeapon->WeaponSetting.AnimWeaponReload,
				CurrentWeapon->WeaponSetting.AnimWeaponReload->SequenceLength / CurrentWeapon->WeaponSetting.ReloadTime);
		}
	}
	else if (CurrentWeapon->WeaponSetting.AnimCharReload)
	{
		WeaponReloadStart_BP(CurrentWeapon->WeaponSetting.AnimCharReload);

		if (!CurrentWeapon->SkeletalOrStatic01 && CurrentWeapon->WeaponSetting.AnimWeaponReloadAim)
		{
			CurrentWeapon->SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(
				CurrentWeapon->WeaponSetting.AnimWeaponReloadAim,
				CurrentWeapon->WeaponSetting.AnimWeaponReloadAim->SequenceLength / CurrentWeapon->WeaponSetting.ReloadTime);
		}
	}
}

void ATDSCharacter::WeaponReloadEnd(bool bIsSuccess, int32 OldRound)
{
	if (InventoryComponent && bIsSuccess && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(
			CurrentWeapon->WeaponSetting.WeaponType,
			OldRound - CurrentWeapon->WeaponInfo.Round);
		InventoryComponent->SetAddicionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

void ATDSCharacter::WeaponFireStart()
{
	if (AimEnable == true && CurrentWeapon->WeaponSetting.AnimCharFireAim)
	{
		WeaponFireStart_BP(CurrentWeapon->WeaponSetting.AnimCharFireAim);
	}
	else if (CurrentWeapon->WeaponSetting.AnimCharFire)
	{
		WeaponFireStart_BP(CurrentWeapon->WeaponSetting.AnimCharFire);
	}

	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAddicionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}
}

void ATDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{

}

void ATDSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{

}

void ATDSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{

}

EPhysicalSurface ATDSCharacter::GetSurfuceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if (HealthComponent)
	{
		if (HealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return Result;
}

void ATDSCharacter::CharDead()
{
	if (bIsAlive)
	{
		float TimeAnim = 0.0f;
		int32 rnd = FMath::RandHelper(DeadsAnim.Num());
		if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
		{
			TimeAnim = DeadsAnim[rnd]->GetPlayLength();
			GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
		}
		bIsAlive = false;

		UnPossessed();

		GetWorldTimerManager().SetTimer(
			TimerHandle_RagDollTimer,
			this,
			&ATDSCharacter::EnableRagdoll,
			TimeAnim,
			false);

		GetCursorToWorld()->SetVisibility(false);
	}
}

float ATDSCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		HealthComponent->ChangeHealthValue(-ActualDamage);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfuceType());
		}
	}

	return ActualDamage;
}

void ATDSCharacter::EnableRagdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

void ATDSCharacter::ChangeCanSprint(bool Can)
{
	CanSprint = Can;
	UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::ChangeCanSprint %s"), CanSprint ? TEXT("true") : TEXT("false"));
	ChangeMovementState();
}

