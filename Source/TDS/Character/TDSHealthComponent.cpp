// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Character/TDSHealthComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UTDSHealthComponent::UTDSHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTDSHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTDSHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UTDSHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTDSHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
	OnHealthChange.Broadcast(Health, NewHealth - Health);
}

void UTDSHealthComponent::ChangeHealthValue(float ChangeValue)
{
	float tmp = Health;
	ChangeValue = ChangeValue * CoefDamage;

	tmp += ChangeValue;

	if (tmp > 100.0f)
	{
		Health = 100.0f;
	}
	else if (tmp < 0.01f)
	{
		Health = 0;
		OnHealthChange.Broadcast(Health, ChangeValue);
		OnDead.Broadcast();
	}
	else
	{
		Health = tmp;
	}

	if (ChangeValue < 0)
	{
		CreateSoundAndParticleFX(EffectTakingDamage, SoundTakingDamage, GetOwner()->GetActorLocation(), GetOwner()->GetActorRotation());
	}
	else
	{
		CreateSoundAndParticleFX(EffectHealing, SoundHealing, GetOwner()->GetActorLocation(), GetOwner()->GetActorRotation());
	}

	OnHealthChange.Broadcast(Health, ChangeValue);
}

void UTDSHealthComponent::CreateSoundAndParticleFX(UParticleSystem* Effect, USoundBase* Sound, FVector Location, FRotator Rotation)
{
	if (Sound)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), Sound, Location);
	}

	if (Effect)
	{
		//UParticleSystemComponent* temporaryFX = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Effect, Location, Rotation, true);
		//temporaryFX->SetupAttachment(GetOwner()->GetRootComponent());
		UGameplayStatics::SpawnEmitterAttached(Effect, GetOwner()->GetRootComponent());
	}
}