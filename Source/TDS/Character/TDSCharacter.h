// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "TDS/FuncLibrary/Types.h"
#include "TDS/Weapon/WeaponDefault.h"
#include "TDS/Interface/TDS_IGameActor.h"

#include "TDSCharacter.generated.h"

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter, public ITDS_IGameActor
{
	GENERATED_BODY()
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	ATDSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* newInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UTDSInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UTDSCharHealthComponent* HealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;


	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;
public:
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnable = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnable = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintEnable = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool CanSprint = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bIsAlive = true;



	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		TArray<UAnimMontage*> DeadsAnim;

	FTimerHandle TimerHandle_RagDollTimer;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;

	//������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20, 40, 40);

	UDecalComponent* CurrentCursor = nullptr;

	//������
	AWeaponDefault* CurrentWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;

public:
	UFUNCTION()
		void InputAxisX(float value);

	UFUNCTION()
		void InputAxisY(float value);

	UFUNCTION()
		void InputAttackPressed();

	UFUNCTION()
		void InputAttackReleased();

	//Tick Func
	UFUNCTION()
		void MovementTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAddicionalWeaponInfo WeaponAddicionalInfo, int32 NewCurrentIndexWeapon);

	UFUNCTION()
		void TrySwitchNextWeapon();
	UFUNCTION()
		void TrySwitchPreviosWeapon();

	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	
	UFUNCTION()
		void WeaponReloadStart();

	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 OldRound);

	UFUNCTION()
		void WeaponFireStart();
	
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);


	//Interface
	
	EPhysicalSurface GetSurfuceType() override;
	//TArray<UTPS_StateEffect*> GetAllCurrentEffects() override;
	//void RemoveEffect(UTPS_StateEffect* RemoveEffect)override;
	//void AddEffect(UTPS_StateEffect* newEffect)override;

	//End Interface


	UFUNCTION()
		void CharDead();
	float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	void EnableRagdoll();

	UFUNCTION()
		void ChangeCanSprint(bool Can);

};

