// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS/Character/TDSHealthComponent.h"
#include "TDSCharHealthComponent.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaChange, float, Stamina, float, ChangeValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHavingStamina, bool, HavingStamina);

UCLASS()
class TDS_API UTDSCharHealthComponent : public UTDSHealthComponent
{
	GENERATED_BODY()

public:

	//SHIELD
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChange OnShieldChange;
	
	FTimerHandle TimerHandle_CollDownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoefShieldDamage = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverRate = 0.1f;

	//STAMINA
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		FOnStaminaChange OnStaminaChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		FOnHavingStamina OnHavingStamina;

	FTimerHandle TimerHandle_CollDownStaminaTimer;
	FTimerHandle TimerHandle_StaminaRecoveryRateTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float MaxStamina = 50.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float CoefStaminaDamage = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoolDownStaminaRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float StaminaRecoverValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float StaminaRecoverRate = 0.1f;




	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		USoundBase* SoundShieldBreaking = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		UParticleSystem* EffectShieldBreaking = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		USoundBase* SoundShieldTakesDamage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		UParticleSystem* EffectShieldTakesDamage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		USoundBase* SoundRecoveryShield = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		UParticleSystem* EffectRecoveryShield = nullptr;

protected:

	float Shield = 100.0f;
	float Stamina = 50.0f;

public:

	void ChangeHealthValue(float ChangeValue) override;

	UFUNCTION(BlueprintCallable, Category = "Shield")
		float GetCurrentShield();

	void ChangeShieldValue(float ChangeValue);
	void CoolDownShieldEnd();
	void RecoveryShield();

	UFUNCTION(BlueprintCallable, Category = "Shield")
		float GetCurrentStamina();
	void ChangeStaminaValue(float RunningTime);
	void CoolDownStaminaEnd();
	void RecoveryStamina();
	
};