// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Character/TDSCharHealthComponent.h"
#include "Kismet/GameplayStatics.h"

void UTDSCharHealthComponent::ChangeHealthValue(float ChangeValue)
{
	if (Shield > 0.01f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
	}
	else if (ChangeValue < 0.0f)
	{
		ChangeShieldValue(0);
		Super::ChangeHealthValue(ChangeValue);
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}


	/*if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
	}
	else
	{
		ChangeShieldValue(0);
		Super::ChangeHealthValue(ChangeValue);
	}*/
}

float UTDSCharHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTDSCharHealthComponent::ChangeShieldValue(float ChangeValue)
{

	if (ChangeValue > 0.01f || ChangeValue < -0.01f)
	{
		float tmp = Shield;
		ChangeValue = ChangeValue * CoefShieldDamage;
		tmp += ChangeValue;

		if (tmp > 100.0f)
		{
			Shield = 100.0f;
		}
		else if (tmp < 0.01f)
		{
			Shield = 0.0f;
			CreateSoundAndParticleFX(EffectShieldBreaking, SoundShieldBreaking, GetOwner()->GetActorLocation(), GetOwner()->GetActorRotation());
			//FX
			//UE_LOG(LogTemp, Warning, TEXT("UTPSCharacterHealthComponent::ChangeHealthValue - Sheild < 0"));
		}
		else
		{
			Shield = tmp;
			
		}

		if (ChangeValue < 0)
		{
			CreateSoundAndParticleFX(EffectShieldTakesDamage, SoundShieldTakesDamage, GetOwner()->GetActorLocation(), GetOwner()->GetActorRotation());
		}
		else
		{
			CreateSoundAndParticleFX(EffectRecoveryShield, SoundRecoveryShield, GetOwner()->GetActorLocation(), GetOwner()->GetActorRotation());
		}

		OnShieldChange.Broadcast(Shield, ChangeValue);
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CollDownShieldTimer);

		GetWorld()->GetTimerManager().SetTimer(
			TimerHandle_CollDownShieldTimer, 
			this, 
			&UTDSCharHealthComponent::CoolDownShieldEnd, 
			CoolDownShieldRecoverTime, 
			false);
	}
}

void UTDSCharHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(
			TimerHandle_ShieldRecoveryRateTimer, 
			this, 
			&UTDSCharHealthComponent::RecoveryShield, 
			ShieldRecoverRate, 
			true);
	}
}

void UTDSCharHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp += ShieldRecoverValue * CoefShieldDamage;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	CreateSoundAndParticleFX(EffectRecoveryShield, SoundRecoveryShield, GetOwner()->GetActorLocation(), GetOwner()->GetActorRotation());

	OnShieldChange.Broadcast(Shield, tmp);
}



float UTDSCharHealthComponent::GetCurrentStamina()
{
	return Stamina;
}

void UTDSCharHealthComponent::ChangeStaminaValue(float RunningTime)
{
	float tmp = Stamina;
	RunningTime = RunningTime * 10 * CoefStaminaDamage;
	tmp += RunningTime;

	if (tmp > 0 && Stamina < 0.01f)
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSCharHealthComponent::ChangeStaminaValue Stamina > 0.01"));
		OnHavingStamina.Broadcast(true);
	}

	if (tmp > MaxStamina)
	{
		Stamina = MaxStamina;
	}
	else if (tmp < 0.01f)
	{
		Stamina = 0.0f;
		//UE_LOG(LogTemp, Warning, TEXT("UTDSCharHealthComponent::ChangeStaminaValue Stamina < 0.01"));
		OnHavingStamina.Broadcast(false);
	}
	else
	{
		Stamina = tmp;

	}

	OnStaminaChange.Broadcast(Stamina, RunningTime);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_StaminaRecoveryRateTimer);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CollDownStaminaTimer);

		GetWorld()->GetTimerManager().SetTimer(
			TimerHandle_CollDownStaminaTimer,
			this,
			&UTDSCharHealthComponent::CoolDownStaminaEnd,
			CoolDownStaminaRecoverTime,
			false);
	}
}

void UTDSCharHealthComponent::CoolDownStaminaEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(
			TimerHandle_StaminaRecoveryRateTimer,
			this,
			&UTDSCharHealthComponent::RecoveryStamina,
			StaminaRecoverRate,
			true);
	}
}

void UTDSCharHealthComponent::RecoveryStamina()
{
	float tmp = Stamina;
	tmp += StaminaRecoverValue * CoefStaminaDamage;

	if (tmp > 0 && Stamina < 0.01f)
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSCharHealthComponent::ChangeStaminaValue Stamina > 0.01"));
		OnHavingStamina.Broadcast(true);
	}

	if (tmp > MaxStamina)
	{
		Stamina = MaxStamina;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_StaminaRecoveryRateTimer);
		}
	}
	else
	{
		Stamina = tmp;
	}

	OnStaminaChange.Broadcast(Stamina, tmp);
}
