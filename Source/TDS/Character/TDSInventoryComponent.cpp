// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Character/TDSInventoryComponent.h"
#include "TDS/Game/TDSGameInstance.h"

// Sets default values for this component's properties
UTDSInventoryComponent::UTDSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTDSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	//Find init weaponsSlots and First Init Weapon
	for (int8 i = 0; i < WeaponSlots.Num(); i++)
	{
		UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
		if (myGI)
		{
			if (!WeaponSlots[i].NameItem.IsNone())
			{
				FWeaponInfo Info;

				if (myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
				{
					//WeaponSlots[i].AddicionalInfo.Round = Info.MaxRound;
				}
				else
				{
					//WeaponSlots.RemoveAt(i);
					//i--;
				}
			}
		}
	}

	MaxSlotsWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
		{
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AddicionalInfo, 0);
		}
	}
	
}


// Called every frame
void UTDSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

FAddicionalWeaponInfo UTDSInventoryComponent::GetAddicionalInfoWeapon(int32 IndexWeapon)
{
	FAddicionalWeaponInfo result;
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		/*int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (/'*WeaponSlots[i].IndexSlot*'/i == IndexWeapon)
			{
				result = WeaponSlots[i].AddicionalInfo;
				bIsFind = true;
			}
			i++;
		}*/
		result = WeaponSlots[IndexWeapon].AddicionalInfo;
		bIsFind = true;
		if (!bIsFind)
		{
			UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);
	}

	return result;
}

int32 UTDSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int32 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFind = true;
			result = i/*WeaponSlots[i].IndexSlot*/;
		}
		i++;
	}
	return result;
}

FName UTDSInventoryComponent::GetWeaponNameBySlotIndex(int32 indexSlot)
{
	FName result;

	if (WeaponSlots.IsValidIndex(indexSlot))
	{
		result = WeaponSlots[indexSlot].NameItem;
	}
	return result;
}

void UTDSInventoryComponent::SetAddicionalInfoWeapon(int32 IndexWeapon, FAddicionalWeaponInfo NewInfo)
{
	int8 SignalIndex = -1; //������, ������� �������������, ��� ������ ������ �� ����� � �������� ������ ����

	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		/*int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				WeaponSlots[i].AddicionalInfo = NewInfo;
				bIsFind = true;
			}
			i++;
		}*/

		WeaponSlots[IndexWeapon].AddicionalInfo = NewInfo;
		bIsFind = true;

		OnWeaponAddicionalInfoChange.Broadcast(IndexWeapon, NewInfo);

		if (!bIsFind)
		{
			UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
		}
	}
	else if (IndexWeapon != SignalIndex)
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);
	}
}

int32 UTDSInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon)
{
	int32 AviableAmmoForWeapon = 0;
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;
			AviableAmmoForWeapon = AmmoSlots[i].Cout;

		}

		i++;
	}

	if (AviableAmmoForWeapon < 1)
	{
		OnWeaponAmmoEmpty.Broadcast(TypeWeapon);//visual empty ammo slot
	}

	return AviableAmmoForWeapon;
}

void UTDSInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AmmoSlots[i].Cout += CoutChangeAmmo;

			if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
			{
				AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;
			}

			OnAmmoChange.Broadcast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);

			bIsFind = true;
		}
		i++;
	}
}

bool UTDSInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool bIsFind = false;
	bool result = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == AmmoType)
		{
			bIsFind = true;
			if (AmmoSlots[i].Cout < AmmoSlots[i].MaxCout)
			{
				result = true;
			}
		}
		i++;
	}
	return result;
}

bool UTDSInventoryComponent::CheckCanTakeWeapon(int32& FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	FWeaponInfo myInfo;
	if (myGI)
	{
		while (i < WeaponSlots.Num() && !bIsFreeSlot)
		{
			if (!myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, myInfo) /*WeaponSlots[i].NameItem.IsNone()*/)
			{
				bIsFreeSlot = true;
				FreeSlot = i;
			}
			i++;
		}
	}

	return bIsFreeSlot;
}

bool UTDSInventoryComponent::SwitchWeaponToInventory(
	FWeaponSlot NewWeapon, 
	int32 IndexSlot, 
	int32 CurrentIndexWeaponChar, 
	FDropItem& DropItemInfo)
{
	bool result = false;
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		FWeaponInfo WeaponInfo;
		if (myGI->GetWeaponInfoByName(NewWeapon.NameItem, WeaponInfo) && 
			WeaponSlots.IsValidIndex(IndexSlot) && 
			GetDropItemInfo(IndexSlot, DropItemInfo))
		{
			WeaponSlots[IndexSlot] = NewWeapon;

			if (IndexSlot == CurrentIndexWeaponChar)
			{
				SwitchWeaponToIndex(CurrentIndexWeaponChar, -1, NewWeapon.AddicionalInfo);
			}

			OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
			result = true;
		}
	}
	return result;
}

bool UTDSInventoryComponent::GetDropItemInfo(int32 IndexSlot, FDropItem& DropItemInfo)
{
	bool result = false;

	FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);

	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		result = myGI->GetDropItemInfoByName(DropItemName, DropItemInfo);
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			DropItemInfo.WeaponInfo.AddicionalInfo = WeaponSlots[IndexSlot].AddicionalInfo;
		}
	}

	return result;
}

bool UTDSInventoryComponent::TryGetWeaponToInventory(FWeaponSlot NewWeapon)
{
	int32 indexSlot = -1;
	if (CheckCanTakeWeapon(indexSlot))
	{
		if (WeaponSlots.IsValidIndex(indexSlot))
		{
			WeaponSlots[indexSlot] = NewWeapon;
			OnUpdateWeaponSlots.Broadcast(indexSlot, NewWeapon);

			return true;
		}
	}
	return false;
}

bool UTDSInventoryComponent::SwitchWeaponToIndex(
	int32 ChangeToIndex,
	int32 OldIndex,
	FAddicionalWeaponInfo OldInfo)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;

	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		int8 NumberOfAttempts = 1;
		while (NumberOfAttempts < WeaponSlots.Num() && !bIsSuccess)
		{
			if ((ChangeToIndex - WeaponSlots.Num()) >= 0)
			{
				CorrectIndex = ChangeToIndex - WeaponSlots.Num();
			}
			else if (ChangeToIndex < 0)
			{
				CorrectIndex = WeaponSlots.Num() + ChangeToIndex;
			}
			else
			{
				CorrectIndex = ChangeToIndex;
			}

			//FName NewIdWeapon;
			//FAddicionalWeaponInfo NewAddicionalInfo;

			/*int8 i = 0;
			while (i < WeaponSlots.Num() && !bIsSuccess)
			{
				if (i == CorrectIndex)
				{
					if (!WeaponSlots[i].NameItem.IsNone())
					{
						NewIdWeapon = WeaponSlots[i].NameItem;
						NewAddicionalInfo = WeaponSlots[i].AddicionalInfo;
						bIsSuccess = true;
					}
				}
				i++;
			}*/

			{
				FWeaponInfo myInfo;
				if (myGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, myInfo))
				{
					if (WeaponSlots[CorrectIndex].AddicionalInfo.Round > 0)
					{
						bIsSuccess = true;
					}
					else
					{
						bool bIsFind = false;
						int32 i = 0;
						while (i < AmmoSlots.Num() && !bIsFind)
						{
							if (AmmoSlots[i].WeaponType == myInfo.WeaponType)
							{
								bIsFind = true;
								if (AmmoSlots[i].Cout > 0)
								{
									bIsSuccess = true;
								}
							}

							i++;
						}
					}
				}
			}

			if (!bIsSuccess)
			{
				if (ChangeToIndex > OldIndex)
				{
					ChangeToIndex++;
				}
				else
				{
					ChangeToIndex--;
				}
			}

			NumberOfAttempts++;
		}
	}

	/*if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
	{
		if (WeaponSlots[CorrectIndex].AddicionalInfo.Round > 0)
		{
			bIsSuccess = true;
		}
		else
		{
				FWeaponInfo myInfo;
				if (myGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, myInfo))
				{
					bool bIsFind = false;
					int32 i = 0;
					while (i < AmmoSlots.Num() && !bIsFind)
					{
						if (AmmoSlots[i].WeaponType == myInfo.WeaponType)
						{
							bIsFind = true;
							if (AmmoSlots[i].Cout > 0)
							{
								bIsSuccess = true;
							}
						}

						i++;
					}
				}
		}
	}*/

	if (bIsSuccess)
	{
		FName NewIdWeapon;
		FAddicionalWeaponInfo NewAddicionalInfo;
		NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
		NewAddicionalInfo = WeaponSlots[CorrectIndex].AddicionalInfo;
		SetAddicionalInfoWeapon(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAddicionalInfo, CorrectIndex);
	}

	/*if (!bIsSuccess && NumberOfAttempts < (WeaponSlots.Num() - 1))
	{
		if (ChangeToIndex > OldIndex)
		{
			ChangeToIndex++;
		}
		else
		{
			ChangeToIndex--;
		}

		bIsSuccess = SwitchWeaponToIndex(ChangeToIndex, OldIndex, OldInfo, NumberOfAttempts + 1, myGI);
	}*/

	return bIsSuccess;
}

